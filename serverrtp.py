#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import socket
# para recibir datos RTP importamos lo siguiente:
import socketserver
import time
import threading


IP = "127.0.0.1"
PORT = 20354    # puerto donde esta esperando recibir los paquetes RTP
# $ python3 serverrtp.py 127.0.0.1:6001 servidor1
# $ python3 serverrtp.py <IPServidorSIP>:<puertoServidorSIP> <servicio>


def get_time():
    # para los historicos
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f'{year}{month}{day}{hour}{minute}{second}'


def get_arguments():
    # $ python3 serverrtp.py 127.0.0.1:6001 servidor1
    if len(sys.argv) != 3:
        sys.exit('Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service>')
    try:
        IPServerSIP = sys.argv[1].split(':')[0]    # dirección IP de la máquina donde está ServidorSIP
        portServerSIP = int(sys.argv[1].split(':')[1])    # puerto UDP donde está escuchando ServidorSIP
        service = sys.argv[2]    # usuario del servidorRTP
    except ValueError:
        sys.exit('Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service>')

    return IPServerSIP, portServerSIP, service


class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes RTP que nos lleguen.

    Atención: Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente, algo que aquí no
    queremos hacer. Al usar BaseRequestHandler no podemos usar self.rfile
    (porque esta clase no lo tiene), así que leemos el mensaje directamente
    de self.request (que es una lista, cuyo primer elemento es el mensaje
    recibido)"""

    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        print("Received")

    @classmethod
    def open_output(cls, filename):
        """Abrir el fichero donde se va a escribir lo recibido.

        Lo abrimos en modo escritura (w) y binario (b)
        Es un método de la clase para que podamos invocarlo sobre la clase
        antes de empezar a recibir paquetes."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.output.close()


def main():
    IPServerSIP, portServerSIP, service = get_arguments()

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f'{get_time()} Starting...')  # Cuando un programa empieza. Histórico

            # REGISTER sip:servidor1@songs.net SIP/2.0\r\n\r\n nada mas arranque el serverRTP
            register = f'REGISTER sip:{service}@songs.net SIP/2.0\r\n\r\n'
            my_socket.sendto(register.encode('utf-8'), (IPServerSIP, portServerSIP))
            print(f'{get_time()} SIP to {IPServerSIP}:{portServerSIP}: {register}')    # Histórico

            data, address = my_socket.recvfrom(1024)    # lo que recibe el cliente del servidorSIP
            print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')    # Histórico

            if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 200 OK':    # si se registra correctamente
                data2, address = my_socket.recvfrom(1024)    # esperamos que lleguen peticiones
                print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data2.decode("utf-8")}')    # Histórico

                if data2.decode('utf-8').split()[0] == 'INVITE':    # si nos llega un INVITE
                    # ---- Datos del client.py ----
                    client_ip = data2.decode('utf-8').split('o=')[1].split()[1]
                    client_address = data2.decode('utf-8').split('sip:')[2].split('>')[0]
                    client_session = data2.decode('utf-8').split('s=')[1].split('\n')[0]

                    # SIP/2.0 200 OK
                    # From: <sip:servidor1@songs.net>
                    # Content-Type: application/sdp
                    #
                    # v=0
                    # o=sip:cliente1@clientes.net 127.0.0.1
                    # s=cliente1
                    # t=0
                    # m=audio 20354 RTP
                    response = f'SIP/2.0 200 OK'
                    head = f'From: <sip:{service}@songs.net>'
                    content_type = f'Content-Type: application/sdp'
                    body = f'v=0\no=sip:{client_address} {client_ip}\ns={client_session}\nt=0\nm=audio {PORT} RTP'
                    content_length = f'Content-Length: {len(body.encode("utf-8"))}'
                    message = f'{response}\r\n{head}\r\n{content_type}\r\n{content_length}\r\n\r\n{body}'
                    my_socket.sendto(message.encode('utf-8'), (address[0], address[1]))
                    print(f'{get_time()} SIP to {address[0]}:{address[1]}: {message}')    # Histórico

                    # ---- Para recibir paquetes RTP ----
                    print(f'{get_time()} RTP ready {PORT}')    # Histórico

                    RTPHandler.open_output('recibido.mp3')
                    with socketserver.UDPServer((IP, PORT), RTPHandler) as serv:
                        print("Listening...")
                        threading.Thread(target=serv.serve_forever).start()
                        time.sleep(30)
                        print("Time passed, shutting down receiver loop.")

                    data3, address = my_socket.recvfrom(1024)    # Respuesta del cliente cuando acaba el envio RTP
                    print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data3.decode("utf-8")}')    # Histórico

                    # si envia un BYE se cierra la comunicacion
                    if data3.decode('utf-8').split()[0] == 'BYE':
                        print(f'{get_time()} RTP all content received {PORT}')    # Histórico
                        serv.shutdown()
                        RTPHandler.close_output()

                        # SIP/2.0 200 OK
                        # From: <sip:servidor1@songs.net>\r\n\r\n
                        assent = f'SIP/2.0 200 OK'
                        head = f'From: <sip:{service}@songs.net>'
                        message = f'{assent}\r\n{head}\r\n\r\n'
                        my_socket.sendto(message.encode('utf-8'), (address[0], address[1]))
                        print(f'{get_time()} SIP to {address[0]}:{address[1]}: {message}')    # Histórico

    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")


if __name__ == "__main__":
    main()
