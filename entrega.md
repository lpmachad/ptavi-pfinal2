## Parte básica
1. Observaciones en el clienty.py
- Cuando recibimos la direccion real del servidor RTP, luego el 200 OK y comienza la comunicacion entre cliente y servidor RTP. Se indica en el enunciado que se debe enviar el ACK a la direccion SIP. No entiendo por que se envia al SIP y no al servidor RTP, ya que ahora la comunicacion es entre cliente y servidor RTP.
2. Observaciones en el servidor SIP
- Cuando mandamos los mensajes de REGISTER los puertos observador en el registro no coinciden con lo que indicamos en cada programa, pero lo he solventado y la comunicacion se realiza sin problema.
3. Observaciones en el servidor RTP
- He creado variables globales cuando obtengo la IP y el puerto del cliente para usarlas en el resto del programa.

## Parte adicional
- Cabecera de tamaño: he añadido esta cabecera en los mensajes de SIP en client.py y serverrtp con cuerpo.

## Otra:
- He tenido que cambiar la cancion.mp3 por la de la entrega pfinal, porque con el audio de pfinal2 tenia errores externos a mis programas.
