#!/usr/bin/python3
# -*- coding: utf-8 -*-


import sys
import socket
# para enviar el file necesitamos importar lo siguiente:
import time
import simplertp


IP = '127.0.0.1'
PORT = 34543
# $ python3 client.py 127.0.0.1:6001 sip:cliente1@clientes.net sip:servidor1@songs.net miaudio.mp3
# $ python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <file>


def get_time():
    # para los historicos
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f'{year}{month}{day}{hour}{minute}{second}'


def get_arguments():
    # $ python3 client.py 127.0.0.1:6001 sip:cliente1@clientes.net sip:servidor1@songs.net miaudio.mp3
    if len(sys.argv) != 5:
        sys.exit('Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <file>')
    try:
        IPServerSIP = sys.argv[1].split(':')[0]  # direccion IP de la maquina donde esta el ServidorSIP
        portServerSIP = int(sys.argv[1].split(':')[1])  # puerto UDP donde esta escuchando ServidorSIP
        addrClient = sys.argv[2]  # direccion SIP cliente para registrarse
        nameAddrClient = sys.argv[2].split(':')[1].split('@')[0]    # nombre cliente cliente1
        addrServerRTP = sys.argv[3]  # direccion SIP ServidorRTP completa
        nameAddrServerRTP = sys.argv[3].split(':')[1].split('@')[0]  # nombre servidorRTP servidor1
        file = sys.argv[4]  # audio para enviar al ServidorRTP
    except ValueError:
        sys.exit('Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <file>')

    return IPServerSIP, portServerSIP, addrClient, nameAddrClient, addrServerRTP, nameAddrServerRTP, file


def main():
    IPServerSIP, portServerSIP, addrClient, nameAddrClient, addrServerRTP, nameAddrServerRTP, file = get_arguments()

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f'{get_time()} Starting...')    # Cuando un programa empieza. Histórico

            # REGISTER sip:cliente1@clientes.net SIP/2.0 nada mas arranque el cliente
            register = f'REGISTER {addrClient} SIP/2.0\r\n\r\n'
            my_socket.sendto(register.encode('utf-8'), (IPServerSIP, portServerSIP))
            print(f'{get_time()} SIP to {IPServerSIP}:{portServerSIP}: {register}')    # Histórico

            data, address = my_socket.recvfrom(1024)    # lo que recibe el cliente del servidorSIP
            print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')    # Histórico

            if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 200 OK':    # si recibe un 200 OK se envia un INVITE
                # INVITE sip:servidor1@songs.net SIP/2.0
                # From: <sip:cliente1@clientes.net>
                # Content-Type: application/sdp
                #
                # v=0
                # o=sip:cliente1@clientes.net 127.0.0.1
                # s=cliente1
                # t=0
                # m=audio 34543 RTP
                invite = f'INVITE {addrServerRTP} SIP/2.0'
                head = f'From: <{addrClient}>'
                content_type = f'Content-Type: application/sdp'
                body = f'v=0\no={addrClient} {IP}\ns={nameAddrClient}\nt=0\nm=audio {PORT} RTP'
                content_length = f'Content-Length: {len(body.encode("utf-8"))}'
                message = f'{invite}\r\n{head}\r\n{content_type}\r\n{content_length}\r\n\r\n{body}'
                my_socket.sendto(message.encode('utf-8'), (IPServerSIP, portServerSIP))
                print(f'{get_time()} SIP to {address[0]}:{address[1]}: {message}')    # Histórico

                data, address = my_socket.recvfrom(1014)    # respuesta del servidorSIP
                print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')    # Histórico

                # nos indica que nos va a redireccionar a la direccion real del serverRTP
                if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 302 Moved Temporarily':
                    # ACK sip: servidor1@songs.net SIP/2.0
                    # From: <sip:cliente1@clientes.net>\r\n\r\n
                    assent_sip = f'ACK {addrServerRTP} SIP/2.0'
                    head = f'From: <{addrClient}>'
                    message_sip = f'{assent_sip}\r\n{head}\r\n\r\n'
                    my_socket.sendto(message_sip.encode('utf-8'), (IPServerSIP, portServerSIP))
                    print(f'{get_time()} SIP to {IPServerSIP}:{portServerSIP}: {message_sip}')    # Histórico

                    # ---- Datos Servidor RTP ----
                    # Necesitamos la IP y el puerto del servidor RTP

                    # SIP/2.0 302 Moved Temporarily\r\n
                    # Contact: sip:servidor1@127.0.0.1.16:20354\r\n\r\n
                    IPServerRTP = data.decode('utf-8').split('\r\n')[1].split()[1].split('@')[1].split(':')[0]
                    portServerRTP_no_init = data.decode('utf-8').split('\r\n')[1].split()[1].split('@')[1].split(':')[1]
                    portServerRTP = int(portServerRTP_no_init)

                    # ---- Mensajes para el Servidor RTP ----
                    # INVITE sip:servidor1@127.0.0.1.16:20354 SIP/2.0
                    # From: <sip:cliente1@clientes.net>
                    # Content-Type: application/sdp
                    #
                    # v=0
                    # o=sip:cliente1@clientes.net 127.0.0.1
                    # s=cliente1
                    # t=0
                    # m=audio 34543 RTP
                    invite_rtp = f'INVITE sip:{nameAddrServerRTP}@{IPServerRTP}:{portServerRTP} SIP/2.0'
                    content_type = f'Content-Type: application/sdp'
                    body = f'v=0\no={addrClient} {IP}\ns={nameAddrClient}\nt=0\nm=audio {PORT} RTP'
                    content_length = f'Content-Length: {len(body.encode("utf-8"))}'
                    message_rtp = f'{invite_rtp}\r\n{head}\r\n{content_type}\r\n{content_length}\r\n\r\n{body}'
                    my_socket.sendto(message_rtp.encode('utf-8'), (IPServerRTP, portServerRTP))
                    print(f'{get_time()} SIP to {IPServerRTP}:{portServerRTP}: {message_rtp}')    # Histórico

                    data, address = my_socket.recvfrom(1024)    # Respuesta del Servidor RTP
                    print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')    # Histórico

                    if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 200 OK':    # si acepta el INVITE
                        # ACK sip:servidor1@127.0.0.1.16:20354 SIP/2.0
                        # From: <sip:cliente1@clientes.net>\r\n\r\n
                        assent = f'ACK sip:{nameAddrServerRTP}@{IPServerRTP}:{portServerRTP} SIP/2.0'
                        head = f'From: <{addrClient}>'
                        message_ack = f'{assent}\n{head}\r\n\r\n'
                        # IMPORTANTE envia el ACK al SIP segun el enunciado
                        my_socket.sendto(message_ack.encode('utf-8'), (IPServerSIP, portServerSIP))
                        print(f'{get_time()} SIP to {IPServerSIP}:{portServerSIP}: {message_ack}')    # Histórico

                        # Necesitamos IP y puerto de ServerRTP para el envio RTP
                        received = data.decode('utf-8')
                        IPServer_RTP = received.split('o=')[1].split()[1]
                        portServer_RTP = int(received.split('audio ')[1].split()[0])

                        # ---- Envio de paquetes RTP ----
                        print(f'{get_time()} RTP to {IPServer_RTP}:{portServer_RTP}')    # Histórico
                        sender = simplertp.RTPSender(ip=IPServer_RTP, port=portServer_RTP,
                                                     file='cancion.mp3', printout=True)
                        sender.send_threaded()
                        time.sleep(5)
                        print("Finalizando el thread de envío.")
                        sender.finish()

                        # Cliente cuando acabe de enviar el fichero envia un BYE
                        # BYE sip:servidor1@s@192.168.10.16:53001 SIP/2.0\r\n\r\n
                        bye = f'BYE sip:{nameAddrServerRTP}@{IPServerRTP}:{portServerRTP} SIP/2.0\r\n\r\n'
                        my_socket.sendto(bye.encode('utf-8'), (IPServerRTP, portServerRTP))
                        print(f'{get_time()} SIP to {IPServerRTP}:{portServerRTP}: {bye}')

                        data, address = my_socket.recvfrom(1024)    # Respuesta del servidor RTP
                        print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')

                        # Si recibimos OK y cerramos la conexion
                        if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 200 OK':
                            my_socket.close()

    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
